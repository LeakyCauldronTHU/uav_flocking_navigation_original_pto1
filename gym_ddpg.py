from Env_2D import EnvUAV
from ddpg import *

EPISODES = 100000
STEPS = 500
TEST = 200
REPLAY_START_SIZE = 5000
NUM_AGENTS = 3
SAVE_RATE = 1000

def main():
    env = EnvUAV(NUM_AGENTS)
    agent = DDPG(env)
    Flag = False
    start_training = 0
    train_step = 0
    episode_rewards = []
    for episode in range(EPISODES):
        state = env.reset()
        # Train
        # print('agents start collecting episode '+str(episode))
        for step in range(STEPS):
            train_step = train_step + 1
            action = agent.behavior_action(state)
            next_state, reward, done, _ = env.forward(action)
            # env.render()
            for i in range(NUM_AGENTS):
                episode_rewards.append(reward[i])
                agent.perceive(state[i], action[i], reward[i], next_state[i], done[i])
            
            if train_step % SAVE_RATE == 0:
                # agent.save()
                File_reward = open('./Result/reward.txt', 'a')
                File_reward.write(str(train_step))
                File_reward.write(' ')
                File_reward.write(str(np.mean(episode_rewards)))
                File_reward.write('\n')
                File_reward.close()
                episode_rewards = []

            if done.any():
                break
            state = next_state
            
        # if (train_step > REPLAY_START_SIZE) and (not start_training):
        #     start_training = episode
        #     Flag = True

        # # Testing:
        # File_reward = open('./Result/reward.txt', 'a')
        # if (episode-start_training) % 5 == 0 and Flag:
        #     total_reward = 0
        #     for i in range(TEST):
        #         state = env.reset()
        #         for j in range(200):
        #             # env.render()
        #             action = agent.test_action(state) # direct action for test
        #             state, reward, done, _ = env.forward(action)
        #             total_reward += np.mean(reward)
        #             if done.any():
        #                 break
        #     ave_reward = total_reward/TEST
        #     File_reward.write(str(ave_reward))
        #     File_reward.write(' ')
        #     File_reward.write(str(episode-start_training))
        #     File_reward.write('\n')
        # File_reward.close()
        
        
if __name__ == '__main__':
    main()

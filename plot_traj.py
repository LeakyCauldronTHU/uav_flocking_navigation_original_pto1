import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D


def draw_cylinder(position):
    return


if __name__ == "__main__":
    mat_height = np.loadtxt('./Result/mat_height.txt')
    mat_exist = np.loadtxt('./Result/mat_exist.txt')
    num_agents = 3
    color = ['b', 'g', 'r', 'c', 'm', 'y', 'k'] * 2
    
    # 绘制地图
    # fig = plt.figure(figsize=(5, 5))
    fig = plt.figure()

    M, N = np.shape(mat_height)
    for i in range(0, 10):
        for j in range(0, 10):
            if mat_exist[i+8, j+8] > 0:
                centroid = [i * 200 + 100, j * 200 + 100]
                color_index = int((mat_height[i+8, j+8] - 30) // 17)
                circle = plt.Circle((centroid[0], centroid[1]), 60, color=color[color_index], fill=True)
                plt.gcf().gca().add_artist(circle)

    # plt.axis('equal')
    plt.xlim(0, 2000)
    plt.ylim(0, 2000)
    
    # 绘制轨迹
    for n in range(num_agents):
        traj = np.loadtxt('./Result/path_agent'+str(n)+'.txt')
        traj_length = np.shape(traj)[0]
        for i in range(traj_length):
            plt.scatter(traj[i, 2], traj[i, 3], color=color[n], linewidths=0.5)

    target = traj[0, 0:2]
    plt.scatter(target[0], target[1], color=color[-1], linewidths=10)
    
    plt.show()
    # exit(0)
